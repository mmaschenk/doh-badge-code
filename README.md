<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="/mmaschenk/doh-badge-code">
    <img src="img/doh-logo.svg" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">DOH-Badge-Code</h3>

  <p align="center">
  Professional coding for beginners!    
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#prerequisites">Prerequisites</a>
      <ul>
        <li><a href="#connecting-the-sensor-to-the-badge">Connecting the sensor to the badge</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#background">Background</a>
      <ul>
        <li><a href="#the-twisted-cable">The twisted cable</a></li>
        <li><a href="#the-color-sensor">The color sensor</a></li>
      </ul>
    </li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->

## About The Project

This is an example repository for running the demo project code for the Delft OpenHardware badge (https://gitlab.com/go-commons/delftopenhardware/doh-badge).

The source is provided as a platformio-ready project.

This project is meant to introduce you to coding for ESP-like products (such as the DOH-Badge)

[![Product Name Screen Shot][doh-screenshot]](https://gitlab.com/go-commons/delftopenhardware/doh-badge)


## Getting started

This project uses the following external products:

* [Visual Studio Code](https://code.visualstudio.com/)
* [PlatformIO](https://platformio.org/)
* [Git](https://gitforwindows.org)

More information is provided further on in this document


This instruction will help you setting up a development environment based on Visual Studio Code and PlatformIO. Currently instructions only apply to Windows 10 clients.

## Prerequisites

### Connecting the sensor to the badge

The code in this repository expects your badge to be connected to a TCS230/TCS3200 Color Sensor. For the demo session, you should have received this sensor and two ribbon cables with which to connect the badge to the color sensor. One of these cables runs all the wires straight from one end to the other, the other cable has a twist in some of the wires. It is important not to mix up these cables since then the color sensor will not work.

The connectors on the badge are marked **J2**, **J3** and **J4**. For the demonstration workshop, only connectors **J2** and **J4** are needed.

The connectors on the color sensor are marked **Sensor** and **Color**.

Use the straight and twisted cable to connect the badge and color sensor in the following way:

|Badge connector | Breakout connector |
|-|-|
|**J4** (Twisted end of twisted cable) | **Sensor** (Straight end of twisted cable)|
|**J2** (Straight cable)| **Color** (Straight cable) |



### Installation

* Mention CH340C driver here??



1. If you haven't done so already, download and install git from gitforwindows.org.
1. If you haven't done so already, download and install Visual Studio Code from code.visualstudio.com.

1. Start Visual Studio Code and click on the Extensions tab.

1. Search for platformio and install the extension.
   ![platformio installation](vid/platformio-install.mp4)

5. Clone this repository to anyplace on your computer where you like to keep source code. 

1. Open this project through platform-io.
1. Click on build and wait for the libraries to be downloaded and the code to build.
1. Attach your badge.
1. Upload the code to your badge.



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request


## Background


### The twisted cable

The twisted cable in the package should twist the wires in the following way:
|  Twisted end | Straight end |
|-------------|--------------|
|pin 1 | pin 4 |
|pin 2| pin 1|
|pin 3|  pin 2|
|pin 4|  pin 5|
|pin 5|  pin 3|

When your color sensor really does not work as expected, check if the cable twists as described here. We are infallible, but that doesn't mean we don't make mistakes :confused:

### The color sensor

If you would like to read more about the color sensor and how it works, here is a great article on that: https://randomnerdtutorials.com/arduino-color-sensor-tcs230-tcs3200/



<!-- LICENSE -->
## License

The source code and contents of this repository are distributed under the Apache 2.0 License. See [LICENSE](LICENSE) for more information.

Please see [the badge repository][doh-url] for the license on the actual hardware and its design.


<!-- CONTACT -->
## Contact

Mark Schenk - [@mmaschenk](https://twitter.com/mmaschenk) - m.m.a.schenk@gmail.com

Project Link: [https://gitlab.com/mmaschenk/doh-badge-code]




<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[doh-screenshot]: https://gitlab.com/go-commons/delftopenhardware/doh-badge/-/raw/master/doh-badge-back-3D.png
[doh-url]: https://gitlab.com/go-commons/delftopenhardware/doh-badge
